// Copyright 2021 Zing Robotics, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ZING_POC_DBW_CAN__ZING_POC_DBW_CAN_HPP_
#define ZING_POC_DBW_CAN__ZING_POC_DBW_CAN_HPP_

#include <can_msgs/msg/frame.hpp>

#include <can_dbc_parser/Dbc.hpp>

#include <string>

#include "zing_poc_dbw_can/can_msgs.hpp"

namespace zing
{
namespace drivers
{

class PocDbwCan
{
public:
  explicit PocDbwCan(const std::string & dbc_path);

  template<class M>
  M from_can_msg(const can_msgs::msg::Frame::SharedPtr msg);

  template<class M>
  can_msgs::msg::Frame to_can_msg(const M msg);

private:
  NewEagle::Dbc m_dbc;
};

}  // namespace drivers
}  // namespace zing

#endif  // ZING_POC_DBW_CAN__ZING_POC_DBW_CAN_HPP_
