// Copyright 2021 Zing Robotics, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ZING_POC_DBW_CAN__ZING_POC_DBW_CAN_NODE_HPP_
#define ZING_POC_DBW_CAN__ZING_POC_DBW_CAN_NODE_HPP_

#include <rclcpp/rclcpp.hpp>
#include <can_msgs/msg/frame.hpp>

#include <memory>
#include <string>
#include <unordered_map>

#include "zing_poc_dbw_can/can_msgs.hpp"
#include "zing_poc_dbw_can/zing_poc_dbw_can.hpp"

namespace zing
{
namespace drivers
{

class PocDbwCanNode : public rclcpp::Node
{
public:
  explicit PocDbwCanNode(const rclcpp::NodeOptions & options);

private:
  std::string m_dbc_path{};
  std::unique_ptr<PocDbwCan> m_can{nullptr};

  template<class T>
  void on_command(const T msg)
  {
    if (m_can) {
      const auto frame = m_can->to_can_msg<T>(msg);
      m_can_pub->publish(frame);
    }
  }

  void on_report(const can_msgs::msg::Frame::SharedPtr msg);

  std::unordered_map<uint32_t, rclcpp::PublisherBase::SharedPtr> m_publishers{};
  std::unordered_map<uint32_t, rclcpp::SubscriptionBase::SharedPtr> m_subscriptions{};

  rclcpp::Publisher<can_msgs::msg::Frame>::SharedPtr m_can_pub{};
  rclcpp::Subscription<can_msgs::msg::Frame>::SharedPtr m_can_sub{};
};

}  // namespace drivers
}  // namespace zing

#endif  // ZING_POC_DBW_CAN__ZING_POC_DBW_CAN_NODE_HPP_
