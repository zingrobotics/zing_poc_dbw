// Copyright 2021 Zing Robotics, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ZING_POC_DBW_CAN__CAN_MSGS_HPP_
#define ZING_POC_DBW_CAN__CAN_MSGS_HPP_

#include <string>
#include <unordered_map>

namespace zing
{
namespace drivers
{

static std::unordered_map<std::string, uint32_t> CAN_MSGS =
{
  {"DBW_SYSTEM_CMD", 0x101},
  {"STEER_CMD", 0x012},
  {"ACCEL_CMD", 0x103},
  {"BRAKE_CMD", 0x104},
  {"DIRECTION_CMD", 0x105},
  {"ACCESSORY_CMD", 0x106},
  {"DBW_SYSTEM_RPT", 0x201},
  {"STEER_RPT", 0x202},
  {"ACCEL_RPT", 0x203},
  {"BRAKE_RPT", 0x204},
  {"DIRECTION_RPT", 0x205},
  {"ACCESSORY_RPT", 0x206}
};

}  // namespace drivers
}  // namespace zing

#endif  // ZING_POC_DBW_CAN__CAN_MSGS_HPP_
