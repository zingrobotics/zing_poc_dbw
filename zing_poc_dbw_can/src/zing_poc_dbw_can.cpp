// Copyright 2021 Zing Robotics, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <zing_poc_dbw_msgs/msg/accel_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/accel_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/accessory_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/accessory_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/brake_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/brake_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/dbw_system_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/dbw_system_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/direction_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/direction_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/steer_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/steer_rpt.hpp>

#include <can_dbc_parser/DbcBuilder.hpp>
#include <can_dbc_parser/DbcMessage.hpp>

#include <string>

#include "zing_poc_dbw_can/zing_poc_dbw_can.hpp"

namespace zing
{
namespace drivers
{

using CanFrame = can_msgs::msg::Frame;
using NewEagle::DbcBuilder;
using NewEagle::DbcMessage;
using zing_poc_dbw_msgs::msg::AccelCmd;
using zing_poc_dbw_msgs::msg::AccelRpt;
using zing_poc_dbw_msgs::msg::AccessoryCmd;
using zing_poc_dbw_msgs::msg::AccessoryRpt;
using zing_poc_dbw_msgs::msg::BrakeCmd;
using zing_poc_dbw_msgs::msg::BrakeRpt;
using zing_poc_dbw_msgs::msg::DbwSystemCmd;
using zing_poc_dbw_msgs::msg::DbwSystemRpt;
using zing_poc_dbw_msgs::msg::DirectionCmd;
using zing_poc_dbw_msgs::msg::DirectionRpt;
using zing_poc_dbw_msgs::msg::SteerCmd;
using zing_poc_dbw_msgs::msg::SteerRpt;

PocDbwCan::PocDbwCan(const std::string & dbc_path)
{
  m_dbc = DbcBuilder().NewDbc(dbc_path);
}

// Accel

template<>
CanFrame PocDbwCan::to_can_msg(const AccelCmd::SharedPtr msg)
{
  CanFrame frame{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["ACCEL_CMD"]);

  if (can_msg != nullptr) {
    can_msg->GetSignal("ACCEL_PEDAL_POS_CMD")->SetResult(msg->accel_pedal_pos_cmd);

    frame = can_msg->GetFrame();
    frame.header.stamp = msg->timestamp;
  }

  return frame;
}

template<>
AccelRpt PocDbwCan::from_can_msg(const CanFrame::SharedPtr msg)
{
  AccelRpt report{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["ACCEL_RPT"]);

  if (can_msg != nullptr && can_msg->GetDlc() == msg->dlc) {
    can_msg->SetFrame(msg);

    report.accel_pedal_pos = static_cast<float>(can_msg->GetSignal("ACCEL_PEDAL_POS")->GetResult());
    report.accel_fault = can_msg->GetSignal("ACCEL_FAULT")->GetResult() == 0.0 ? false : true;

    report.timestamp = msg->header.stamp;
  }

  return report;
}

// Accessory

template<>
CanFrame PocDbwCan::to_can_msg(const AccessoryCmd::SharedPtr msg)
{
  CanFrame frame{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["ACCESSORY_CMD"]);

  if (can_msg != nullptr) {
    can_msg->GetSignal("PARKING_BRAKE_EN_CMD")->SetResult(msg->parking_brake_en_cmd ? 1 : 0);
    can_msg->GetSignal("LED_MODE_CMD")->SetResult(msg->led_mode_cmd);
    can_msg->GetSignal("BEACON_MODE_CMD")->SetResult(msg->beacon_mode_cmd);

    frame = can_msg->GetFrame();
    frame.header.stamp = msg->timestamp;
  }

  return frame;
}

template<>
AccessoryRpt PocDbwCan::from_can_msg(const CanFrame::SharedPtr msg)
{
  AccessoryRpt report{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["ACCESSORY_RPT"]);

  if (can_msg != nullptr && can_msg->GetDlc() == msg->dlc) {
    can_msg->SetFrame(msg);

    report.parking_brake_en = can_msg->GetSignal("PARKING_BRAKE_EN")->GetResult() ==
      0.0 ? false : true;
    report.led_mode = static_cast<uint8_t>(can_msg->GetSignal("LED_MODE")->GetResult());
    report.beacon_mode = static_cast<uint8_t>(can_msg->GetSignal("BEACON_MODE")->GetResult());

    report.timestamp = msg->header.stamp;
  }

  return report;
}

// Brake

template<>
CanFrame PocDbwCan::to_can_msg(const BrakeCmd::SharedPtr msg)
{
  CanFrame frame{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["BRAKE_CMD"]);

  if (can_msg != nullptr) {
    can_msg->GetSignal("BRAKE_PEDAL_POS_CMD")->SetResult(msg->brake_pedal_pos_cmd);

    frame = can_msg->GetFrame();
    frame.header.stamp = msg->timestamp;
  }

  return frame;
}

template<>
BrakeRpt PocDbwCan::from_can_msg(const CanFrame::SharedPtr msg)
{
  BrakeRpt report{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["BRAKE_RPT"]);

  if (can_msg != nullptr && can_msg->GetDlc() == msg->dlc) {
    can_msg->SetFrame(msg);

    report.brake_pedal_pos = static_cast<float>(can_msg->GetSignal("BRAKE_PEDAL_POS")->GetResult());
    report.brake_fault = can_msg->GetSignal("BRAKE_FAULT")->GetResult() == 0.0 ? false : true;

    report.timestamp = msg->header.stamp;
  }

  return report;
}

// DbwSystem

template<>
CanFrame PocDbwCan::to_can_msg(const DbwSystemCmd::SharedPtr msg)
{
  CanFrame frame{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["DBW_SYSTEM_CMD"]);

  if (can_msg != nullptr) {
    can_msg->GetSignal("MODE_CMD")->SetResult(msg->mode_cmd);

    frame = can_msg->GetFrame();
    frame.header.stamp = msg->timestamp;
  }

  return frame;
}

template<>
DbwSystemRpt PocDbwCan::from_can_msg(const CanFrame::SharedPtr msg)
{
  DbwSystemRpt report{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["DBW_SYSTEM_RPT"]);

  if (can_msg != nullptr && can_msg->GetDlc() == msg->dlc) {
    can_msg->SetFrame(msg);

    report.mode = static_cast<uint8_t>(can_msg->GetSignal("MODE")->GetResult());
    report.ready_for_cmd = can_msg->GetSignal("READY_FOR_CMD")->GetResult() == 0.0 ? false : true;
    report.ign_switch_on = can_msg->GetSignal("IGN_SWITCH_ON")->GetResult() == 0.0 ? false : true;

    report.timestamp = msg->header.stamp;
  }

  return report;
}

// Direction

template<>
CanFrame PocDbwCan::to_can_msg(const DirectionCmd::SharedPtr msg)
{
  CanFrame frame{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["DIRECTION_CMD"]);

  if (can_msg != nullptr) {
    can_msg->GetSignal("DIRECTION_CMD")->SetResult(msg->direction_cmd);

    frame = can_msg->GetFrame();
    frame.header.stamp = msg->timestamp;
  }

  return frame;
}

template<>
DirectionRpt PocDbwCan::from_can_msg(const CanFrame::SharedPtr msg)
{
  DirectionRpt report{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["DIRECTION_RPT"]);

  if (can_msg != nullptr && can_msg->GetDlc() == msg->dlc) {
    can_msg->SetFrame(msg);

    report.direction = static_cast<uint8_t>(can_msg->GetSignal("DIRECTION")->GetResult());

    report.timestamp = msg->header.stamp;
  }

  return report;
}

// Steer

template<>
CanFrame PocDbwCan::to_can_msg(const SteerCmd::SharedPtr msg)
{
  CanFrame frame{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["STEER_CMD"]);

  if (can_msg != nullptr) {
    can_msg->GetSignal("STEER_POS_CMD")->SetResult(msg->steer_pos_cmd);
    can_msg->GetSignal("MAX_ROT_RATE")->SetResult(msg->max_rot_rate);

    frame = can_msg->GetFrame();
    frame.header.stamp = msg->timestamp;
  }

  return frame;
}

template<>
SteerRpt PocDbwCan::from_can_msg(const CanFrame::SharedPtr msg)
{
  SteerRpt report{};
  auto can_msg = m_dbc.GetMessageById(CAN_MSGS["STEER_RPT"]);

  if (can_msg != nullptr && can_msg->GetDlc() == msg->dlc) {
    can_msg->SetFrame(msg);

    report.steer_pos = static_cast<float>(can_msg->GetSignal("STEER_POS")->GetResult());
    report.rot_rate = static_cast<float>(can_msg->GetSignal("ROT_RATE")->GetResult());
    report.steer_fault = can_msg->GetSignal("STEER_FAULT")->GetResult() == 0.0 ? false : true;

    report.timestamp = msg->header.stamp;
  }

  return report;
}

}  // namespace drivers
}  // namespace zing
