// Copyright 2021 Zing Robotics, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <zing_poc_dbw_msgs/msg/accel_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/accel_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/accessory_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/accessory_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/brake_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/brake_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/dbw_system_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/dbw_system_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/direction_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/direction_rpt.hpp>
#include <zing_poc_dbw_msgs/msg/steer_cmd.hpp>
#include <zing_poc_dbw_msgs/msg/steer_rpt.hpp>

#include <memory>
#include <string>

#include "zing_poc_dbw_can/zing_poc_dbw_can.hpp"
#include "zing_poc_dbw_can/zing_poc_dbw_can_node.hpp"

namespace zing
{
namespace drivers
{

using zing_poc_dbw_msgs::msg::AccelCmd;
using zing_poc_dbw_msgs::msg::AccelRpt;
using zing_poc_dbw_msgs::msg::AccessoryCmd;
using zing_poc_dbw_msgs::msg::AccessoryRpt;
using zing_poc_dbw_msgs::msg::BrakeCmd;
using zing_poc_dbw_msgs::msg::BrakeRpt;
using zing_poc_dbw_msgs::msg::DbwSystemCmd;
using zing_poc_dbw_msgs::msg::DbwSystemRpt;
using zing_poc_dbw_msgs::msg::DirectionCmd;
using zing_poc_dbw_msgs::msg::DirectionRpt;
using zing_poc_dbw_msgs::msg::SteerCmd;
using zing_poc_dbw_msgs::msg::SteerRpt;

PocDbwCanNode::PocDbwCanNode(const rclcpp::NodeOptions & options)
: rclcpp::Node("zing_poc_dbw_can_node", options)
{
  m_dbc_path = declare_parameter("dbc_path").get<std::string>();

  m_can = std::make_unique<PocDbwCan>(m_dbc_path);

  m_publishers[CAN_MSGS["DBW_SYSTEM_RPT"]] =
    create_publisher<DbwSystemRpt>("dbw_system_rpt", rclcpp::QoS{10});
  m_publishers[CAN_MSGS["STEER_RPT"]] =
    create_publisher<SteerRpt>("steer_rpt", rclcpp::QoS{10});
  m_publishers[CAN_MSGS["ACCEL_RPT"]] =
    create_publisher<AccelRpt>("accel_rpt", rclcpp::QoS{10});
  m_publishers[CAN_MSGS["BRAKE_RPT"]] =
    create_publisher<BrakeRpt>("brake_rpt", rclcpp::QoS{10});
  m_publishers[CAN_MSGS["DIRECTION_RPT"]] =
    create_publisher<DirectionRpt>("direction_rpt", rclcpp::QoS{10});
  m_publishers[CAN_MSGS["ACCESSORY_RPT"]] =
    create_publisher<AccessoryRpt>("accessory_rpt", rclcpp::QoS{10});

  m_subscriptions[CAN_MSGS["DBW_SYSTEM_CMD"]] =
    create_subscription<DbwSystemCmd>(
    "dbw_system_cmd",
    rclcpp::QoS{10},
    std::bind(&PocDbwCanNode::on_command<DbwSystemCmd::SharedPtr>, this, std::placeholders::_1));

  m_subscriptions[CAN_MSGS["STEER_CMD"]] =
    create_subscription<SteerCmd>(
    "steer_cmd",
    rclcpp::QoS{10},
    std::bind(&PocDbwCanNode::on_command<SteerCmd::SharedPtr>, this, std::placeholders::_1));

  m_subscriptions[CAN_MSGS["ACCEL_CMD"]] =
    create_subscription<AccelCmd>(
    "accel_cmd",
    rclcpp::QoS{10},
    std::bind(&PocDbwCanNode::on_command<SteerCmd::SharedPtr>, this, std::placeholders::_1));

  m_subscriptions[CAN_MSGS["BRAKE_CMD"]] =
    create_subscription<BrakeCmd>(
    "brake_cmd",
    rclcpp::QoS{10},
    std::bind(&PocDbwCanNode::on_command<BrakeCmd::SharedPtr>, this, std::placeholders::_1));

  m_subscriptions[CAN_MSGS["DIRECTION_CMD"]] =
    create_subscription<DirectionCmd>(
    "direction_cmd",
    rclcpp::QoS{10},
    std::bind(&PocDbwCanNode::on_command<DirectionCmd::SharedPtr>, this, std::placeholders::_1));

  m_subscriptions[CAN_MSGS["ACCESSORY_CMD"]] =
    create_subscription<AccessoryCmd>(
    "accessory_cmd",
    rclcpp::QoS{10},
    std::bind(&PocDbwCanNode::on_command<AccessoryCmd::SharedPtr>, this, std::placeholders::_1));

  m_can_pub = create_publisher<can_msgs::msg::Frame>(
    "to_can_bus", rclcpp::QoS{20});

  m_can_sub = create_subscription<can_msgs::msg::Frame>(
    "from_can_bus", rclcpp::QoS{20},
    std::bind(&PocDbwCanNode::on_report, this, std::placeholders::_1));
}

void PocDbwCanNode::on_report(const can_msgs::msg::Frame::SharedPtr msg)
{
  if (msg->id == CAN_MSGS["DBW_SYSTEM_RPT"]) {
    const auto pub_msg = m_can->from_can_msg<DbwSystemRpt>(msg);
    std::dynamic_pointer_cast<rclcpp::Publisher<DbwSystemRpt>>(
      m_publishers[msg->id])->publish(pub_msg);
  } else if (msg->id == CAN_MSGS["STEER_RPT"]) {
    const auto pub_msg = m_can->from_can_msg<SteerRpt>(msg);
    std::dynamic_pointer_cast<rclcpp::Publisher<SteerRpt>>(
      m_publishers[msg->id])->publish(pub_msg);
  } else if (msg->id == CAN_MSGS["ACCEL_RPT"]) {
    const auto pub_msg = m_can->from_can_msg<AccelRpt>(msg);
    std::dynamic_pointer_cast<rclcpp::Publisher<AccelRpt>>(
      m_publishers[msg->id])->publish(pub_msg);
  } else if (msg->id == CAN_MSGS["BRAKE_RPT"]) {
    const auto pub_msg = m_can->from_can_msg<BrakeRpt>(msg);
    std::dynamic_pointer_cast<rclcpp::Publisher<BrakeRpt>>(
      m_publishers[msg->id])->publish(pub_msg);
  } else if (msg->id == CAN_MSGS["DIRECTION_RPT"]) {
    const auto pub_msg = m_can->from_can_msg<DirectionRpt>(msg);
    std::dynamic_pointer_cast<rclcpp::Publisher<DirectionRpt>>(
      m_publishers[msg->id])->publish(pub_msg);
  } else if (msg->id == CAN_MSGS["ACCESSORY_RPT"]) {
    const auto pub_msg = m_can->from_can_msg<AccessoryRpt>(msg);
    std::dynamic_pointer_cast<rclcpp::Publisher<AccessoryRpt>>(
      m_publishers[msg->id])->publish(pub_msg);
  } else {
    RCLCPP_WARN(get_logger(), "Received unrecognized CAN ID 0x%X", msg->id);
  }
}

}  // namespace drivers
}  // namespace zing

#include <rclcpp_components/register_node_macro.hpp>  // NOLINT
RCLCPP_COMPONENTS_REGISTER_NODE(zing::drivers::PocDbwCanNode)
